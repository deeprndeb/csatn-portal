from django.contrib.auth.decorators import login_required
from .models import (ExamMember, ISO, Asset, Vulnerability, CISO)
from .forms import (ExamMemberForm, ISOForm, AssetForm,
                    VulnerabilityForm, CISOForm)
from django.shortcuts import redirect, render
from login.forms import UserForm, UserProfileInfoForm
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse


import logging
from django.contrib import messages
logger = logging.getLogger('centmonitoringdebug')

alogger = logging.getLogger('accesslog')

elogger = logging.getLogger('errorlog')

ilogger = logging.getLogger('infolog')


def index(request):
    return render(request, 'login/index.html')


@login_required
def special(request):
    return HttpResponse("You are logged in !")


@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('index'))


def register(request):
    registered = False
    if request.method == 'POST':
        user_form = UserForm(data=request.POST)
        if user_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()
            # profile = profile_form.save(commit=False)
            # profile.user = user
            # if 'profile_pic' in request.FILES:
            #     print('found it')
            #     profile.profile_pic = request.FILES['profile_pic']
            # profile.save()
            registered = True
        else:
            print(user_form.errors)
    else:
        user_form = UserForm()
    return render(request, 'login/registration.html',
                  {'user_form': user_form,
                           'registered': registered})


def user_login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        request.session["username"] = username
        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                login(request, user)
                return redirect('/member/exammember')
            else:
                return HttpResponse("Your account was inactive.")
        else:
            print("Someone tried to login and failed.")
            print("They used username: {} and password: {}".format(
                username, password))
            return HttpResponse("Invalid login details given")
    else:
        return render(request, 'login/login.html', {})


@login_required
def Member(request):
    """
    view of member add and mapping with role
    """
    context = {}
    context['member'] = ExamMember.objects.all()
    if request.method == 'POST':
        memberdet = ExamMemberForm(request.POST)
        if memberdet.is_valid():
            try:
                post = memberdet.save(commit=False)
                post.save()
                return redirect('/member/exammember')
            except Exception in det:
                print(det, '=-')
                return render(request, 'login/exammember.html', context)
        else:
            context['form'] = memberdet
            return render(request, 'login/exammember.html', context)
    else:
        context['form'] = ExamMemberForm()
        return render(request, 'login/exammember.html', context)



@login_required
def Member1(request):
    """
    view of member add and mapping with role
    """
    context = {}
    context['iso'] = ISO.objects.all()
    if request.method == 'POST':
        memberdet1 = ISOForm(request.POST)
        if memberdet1.is_valid():
            try:
                post = memberdet1.save(commit=False)
                post.save()
                return redirect('/member/iso')
            except Exception in det:
                print(det, '=-')
                return render(request, 'login/iso.html', context)
        else:
            context['form'] = memberdet1
            return render(request, 'login/iso.html', context)
    else:
        context['form'] = ISOForm()
        return render(request, 'login/iso.html', context)


@login_required
def Member2(request):
    """
    view of member add and mapping with role
    """
    context = {}
    context['asset'] = Asset.objects.all()
    if request.method == 'POST':
        memberdet2 = AssetForm(request.POST)
        if memberdet2.is_valid():
            try:
                post = memberdet2.save(commit=False)
                post.save()
                return redirect('/member/asset')
            except Exception in det:
                print(det, '=-')
                return render(request, 'login/asset.html', context)
        else:
            context['form'] = memberdet2
            return render(request, 'login/asset.html', context)
    else:
        context['form'] = AssetForm()
        return render(request, 'login/asset.html', context)


@login_required
def Member3(request):
    """
    view of member add and mapping with role
    """
    context = {}
    context['vul'] = Vulnerability.objects.all()
    if request.method == 'POST':
        memberdet3 = VulnerabilityForm(request.POST)
        if memberdet3.is_valid():
            try:
                post = memberdet3.save(commit=False)
                post.modified_user = request.user
                post.save()
                return redirect('/member/vul')
            except Exception in det:
                print(det, '=-')
                return render(request, 'login/vulnerability.html', context)
        else:
            context['form'] = memberdet3
            return render(request, 'login/vulnerability.html', context)
    else:
        context['form'] = VulnerabilityForm()
        return render(request, 'login/vulnerability.html', context)


@login_required
def Member_vul(request):
    """
    view of member add and mapping with role
    """
    context = {}
    context['vul1'] = Vulnerability.objects.all()
    if request.method == 'POST':
        memberdet6 = VulnerabilityForm(request.POST)
        if memberdet6.is_valid():
            try:
                post = memberdet6.save(commit=False)
                post.modified_user = request.user
                post.save()
                return redirect('/member/vul1')
            except Exception in det:
                print(det, '=-')
                return render(request, 'login/vul_report.html', context)
        else:
            context['form'] = memberdet6
            return render(request, 'login/vul_report.html', context)
    else:
        context['form'] = VulnerabilityForm()
        return render(request, 'login/vul_report.html', context)


@login_required
def Member4(request):
    """
    view of member add and mapping with role
    """
    context = {}
    context['ciso'] = CISO.objects.all()
    if request.method == 'POST':
        memberdet4 = CISOForm(request.POST)
        if memberdet4.is_valid():
            try:
                post = memberdet4.save(commit=False)
                post.save()
                return redirect('/member/ciso')
            except Exception in det:
                print(det, '=-')
                return render(request, 'login/ciso.html', context)
        else:
            context['form'] = memberdet4
            return render(request, 'login/ciso.html', context)
    else:
        context['form'] = CISOForm()
        return render(request, 'login/ciso.html', context)
