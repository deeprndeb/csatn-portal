from django.conf.urls import url
from django.urls import path
from .member_fetch import load_members

from login import views
from . import datatable
from . import uploadfile


# SET THE NAMESPACE!
app_name = 'login'
# Be careful setting the name to just /login use userlogin instead!
urlpatterns=[
    url(r'^register/$',views.register,name='register'),
    url(r'^user_login/$',views.user_login,name='user_login'),
    #url(r'^login/',include('login.urls'))
    
    path('exammember/', views.Member, name='exammember'),
    path('deletemember/<int:id>', datatable.DeleteMember, name='deletemember'),
    path('editmember/<int:id>', datatable.EditMember, name='editmember'),
    path('updatemember/<int:id>', datatable.UpdateMember, name='updatemember'),
    path('iso/', views.Member1, name='iso'),
    path('asset/', views.Member2, name='asset'),
    path('deleteiso/<int:id>', datatable.DeleteISO, name='deleteiso'),
    path('editiso/<int:id>', datatable.EditISO, name='editiso'),
    path('updateiso/<int:id>', datatable.UpdateISO, name='updateiso'),
    path('deleteasset/<int:id>', datatable.DeleteAsset, name='deleteasset'),
    path('editasset/<int:id>', datatable.EditAsset, name='editasset'),
    path('updateasset/<int:id>', datatable.UpdateAsset, name='updateasset'),
    path('vul/', views.Member3, name='vul'),
    path('vul1/', views.Member_vul, name='vul1'),
    path('editvul1/<int:id>', datatable.EditVul1, name='editvul1'),
    path('updatevul1/<int:id>', datatable.UpdateVul1, name='updatevul1'),
    path('deletevul1/<int:id>', datatable.DeleteVul1, name='deletevul1'),
    path('editvul/<int:id>', datatable.EditVul1, name='editvul'),
    path('updatevul/<int:id>', datatable.UpdateVul, name='updatevul'),
    path('deletevul/<int:id>', datatable.DeleteVul, name='deletevul'),
    path('ciso/', views.Member4, name='ciso'),
    path('editciso/<int:id>', datatable.Editciso, name='editciso'),
    path('updateciso/<int:id>', datatable.Updateciso, name='updateciso'),
    path('deleteciso/<int:id>', datatable.Deleteciso, name='deleteciso'),
    #path('uploadmember/', uploadfile.UploadMemberXls.as_view(), name='uploadmember')
    path('loadmembers/', load_members, name='loadmembers')


]
