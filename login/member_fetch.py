from django.shortcuts import render, redirect
from django.db import connection
from .models import ExamMember, ISO
from django.contrib.auth.decorators import login_required




def load_members(request):
    id_departmentname = request.POST.get('departmentname')
    print(id_departmentname)
    print(ISO.objects.all())
    members = ISO.objects.filter(
        id_departmentname=id_departmentname).order_by('departmentcode')
    print(members)
    return redirect(request, 'login/iso.html', {'members': members})

