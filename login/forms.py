from django import forms
from login.models import UserProfileInfo
from django.contrib.auth.models import User
from django.forms import ModelForm
from login.models import (ExamMember, ISO, Asset, Vulnerability, CISO)

class DateInput(forms.DateInput):
    input_type = 'date'

class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())
    class Meta():
        model = User
        fields = ('username','password','email')

class UserProfileInfoForm(forms.ModelForm):
     class Meta():
         model = UserProfileInfo
         fields = ('portfolio_site','profile_pic')

class ExamMemberForm(ModelForm):
    class Meta:
        model = ExamMember
        fields = "__all__"
        labels = {
            'departmentid': ('departmentid'),
            'departmentcode': ('departmentcode'),
            'organisationaname': ('organisationaname'),
            'departmentname': ('departmentname'),
            'phone_number': ('phone_number'),
            'date': ('date'),
            'email': ('email')


        }
        widgets = {
            'date': DateInput(),
        }

class ISOForm(ModelForm):
    class Meta:
        model = ISO
        fields = "__all__"
        labels = {
            'departmentid': ('departmentid'),		
	        'departmentcode': ('departmentcode'),			
	        'organisationaname': ('organisationaname'),			
	        'departmentname': ('departmentname'),			
	        'nameiso':	('nameiso'),		
	        'designation':	('designation'),		
	        'phone_number':	('phone_number'),		
	        'date':	('date'),		
	        'email': ('email'),			
	        'status': ('status')
        }
        widgets = {
            'date': DateInput(),
        }

class AssetForm(ModelForm):
    class Meta:
        model = Asset
        fields = "__all__"
        labels = {
            'organisationaname': ('organisationaname'),		
	        'departmentname': ('departmentname'),			
	        'url': ('url'),			
	        'tnsdcip': ('tnsdcip'),			
	        'nms':	('nms'),		
	        'nmsdate':	('nmsdate'),		
	        'assetusage':	('assetusage'),		
	        'othersdetails':	('othersdetails'),		
	        'departmentcontact': ('departmentcontact'),			
	        'status': ('status'),
            'departmentcontact': ('departmentcontact'),			
	        'developer': ('developer'),
            'siem': ('siem'),			
	        'siemdate': ('siemdate')
            

        }
        widgets = {
            'date': DateInput(),
            'nmsdate': DateInput(),
            'siemdate': DateInput(),

        }


class VulnerabilityForm(ModelForm):
    class Meta:
        model = Vulnerability
        fields = ['vulid', 'vulname', 'departmentname', 'reportby', 'reporteddate' ]
        labels = {
            'vulid': ('Vulnerability Id'),
            'vulname': ('Vulnerability Name'),
            'departmentname': ('Department Name'),
            'reportby': ('Report By Name'),
            'reporteddate': ('Report Date')
  
        }
        widgets = {
            'reporteddate': DateInput(),
        }  

class CISOForm(ModelForm):
    class Meta:
        model = CISO
        fields = "__all__"
        labels = {
            'departmentid':	('departmentid'),		
	        'departmentcode': ('departmentcode'),			
	        'organisationaname': ('organisationaname'),			
	        'nameciso': ('nameciso'), 			
	        'designation': ('designation'),			
	        'phone_number': ('phone_number'),			
	        'date': ('date'),			
	        'email': ('email'),			
	        'status': ('status')			
	        			
        
        }
        widgets = {
            'date': DateInput(),
        }              
