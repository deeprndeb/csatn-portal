import email
from email.policy import default
from random import choices
from django.db import models
from django.utils import timezone



# Create your models here.
from unicodedata import name
from django.db import models
from django.contrib.auth.models import User
from datetime import datetime
from login.choice import status, shift, dcode
#from django.core.validators import MinLengthValidator
#from django.core import validators
#from .validators import (ContactNumberCheck)
# Create your models here.

class UserProfileInfo(models.Model):
	user = models.OneToOneField(User,on_delete=models.CASCADE)
	portfolio_site = models.CharField(max_length=100)
	profile_pic = models.ImageField(upload_to='profile_pics',blank=True)
def __str__(self):
  return self.username

class ExamMember(models.Model):
	departmentid				= models.CharField(max_length=250)
	departmentcode				= models.CharField(max_length=250)
	organisationaname			= models.CharField(max_length=250)
	departmentname				= models.CharField(max_length=250)
	phone_number				= models.CharField(max_length=10,unique=True)
	date						= models.DateField()
	email						= models.EmailField(max_length = 254)
	modificationdate			= models.DateTimeField(auto_now_add=True)
	# username			= models.ForeignKey(User,on_delete=models.CASCADE)

	def __str__(self):
		return self.departmentname

class ISO(models.Model):
	departmentid			= models.CharField(max_length=250)
	departmentcode			= models.CharField(max_length=250)
	organisationaname		= models.CharField(max_length=250)
	departmentname			= models.ForeignKey(ExamMember,on_delete=models.CASCADE)
	nameiso					= models.CharField(max_length=250)
	designation				= models.CharField(max_length=250)
	phone_number			= models.CharField(max_length=10,unique=True)
	date					= models.DateField()
	email					= models.EmailField(max_length = 254)
	status					= models.CharField(max_length=250,choices=status)
	modificationdate		= models.DateTimeField(auto_now_add=True)
	
	def __str__(self):
		return self.departmentname

class CISO(models.Model):
	departmentid			= models.CharField(max_length=250)
	departmentcode			= models.CharField(max_length=250)
	organisationaname		= models.CharField(max_length=250)
	nameciso				= models.CharField(max_length=250)
	designation				= models.CharField(max_length=250)
	phone_number			= models.CharField(max_length=10,unique=True)
	date					= models.DateField()
	email					= models.EmailField(max_length = 254)
	status					= models.CharField(max_length=250,choices=status)
	modificationdate		= models.DateTimeField(auto_now_add=True)
	
	def __str__(self):
		return self.departmentname

class Asset(models.Model):
	organisationaname			= models.CharField(max_length=250)
	departmentname			= models.ForeignKey(ExamMember,on_delete=models.CASCADE)
	url			= models.CharField(max_length=250)
	tnsdcip			= models.CharField(max_length=250)
	nms			= models.CharField(max_length=250,choices=shift)
	nmsdate			= models.DateField()
	assetusage			= models.CharField(max_length=250)
	othersdetails			= models.CharField(max_length=250)
	departmentcontact			= models.CharField(max_length=10,unique=True)
	developer			= models.CharField(max_length=250)
	siem			= models.CharField(max_length=250,choices=shift)
	siemdate			= models.DateField()
	modificationdate			= models.DateTimeField(auto_now_add=True)
	
	def __str__(self):
		return self.name

class Vulnerability(models.Model):
	departmentname			= models.ForeignKey(ExamMember,on_delete=models.CASCADE)
	departmentcode			= models.CharField(max_length=250)
	vulid			= models.CharField(max_length=250)
	vulname			= models.CharField(max_length=250)
	reportby 			= models.CharField(max_length=200)
	reporteddate 			= models.DateField()
	modificationdate			= models.DateField(auto_now_add=True)
	modified_user	= models.ForeignKey(User, on_delete=models.CASCADE)
	active = models.BooleanField(default=True)

	
	def __str__(self):
		return self.vulid		