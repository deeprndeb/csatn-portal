from django.contrib import admin
from login.models import UserProfileInfo, User, ExamMember, ISO, Asset, Vulnerability, CISO

# Register your models here.



admin.site.register(ExamMember)
admin.site.register(ISO)
admin.site.register(Asset)
admin.site.register(Vulnerability)
admin.site.register(CISO)
admin.site.register(UserProfileInfo)
