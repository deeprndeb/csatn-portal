from datetime import date, datetime
from django.shortcuts import render, redirect, get_object_or_404
from .forms import (ExamMemberForm, ISOForm, AssetForm,
                    VulnerabilityForm, CISOForm)
from django.http import HttpResponse, HttpResponseRedirect
from .models import (CISO, ISO, Asset, ExamMember, Vulnerability, CISO)
from django.contrib import messages
from django.core.exceptions import ValidationError
from django.contrib.auth.decorators import login_required
from django.views.generic.edit import UpdateView
from django.contrib.messages.views import SuccessMessageMixin
import logging

logger = logging.getLogger('centmonitoringdebug')

alogger = logging.getLogger('accesslog')

elogger = logging.getLogger('errorlog')

ilogger = logging.getLogger('infolog')


# Create your views here.
@login_required
def DeleteMember(request, id):
    try:
        record = get_object_or_404(ExamMember, id=id)
        record.delete()
        messages.success(request, '%s member deleted successfully' %
                         (record.organisationaname))
        ilogger.info('member %s deleted by %s' %
                     (record.organisationaname, request.user))
        return HttpResponseRedirect("/member/exammember")
    except Exception as details:
        elogger.error(
            'Trying to delete DeleteMember id: %s no such record' % (id))
        return HttpResponseRedirect("/member/exammember")


@login_required
def EditMember(request, id):
    member = ExamMember.objects.get(id=id)
    initial_dict = {
        'departmentid': member.departmentid,
        'departmentcode': member.departmentcode,
        'organisationaname': member.organisationaname,
        'departmentname': member.departmentname,
        'phone_number': member.phone_number,
        'date': member.date,
        'email': member.email
        }
    # add the dictionary during initialization

    form = ExamMemberForm(request.POST or None, initial=initial_dict)
    context = {}
    context['form'] = form
    context['id'] = id
    context['pageicon'] = "fa-group"
    context['pagetitle'] = "Department Details"
    return render(request, "login/editmember.html", context)


@login_required
def UpdateMember(request, id):
    context = {}
    dbdata = ExamMember.objects.get(id=id)
    try:
        ExamMember.objects.filter(id=id).update(departmentid=request.POST.get('departmentid'),
                                                departmentcode=request.POST.get(
                                                    'departmentcode'),
                                                organisationaname=request.POST.get(
                                                    'organisationaname'),
                                                departmentname=request.POST.get(
                                                    'departmentname'),
                                                phone_number=request.POST.get(
                                                    'phone_number'),
                                                date=request.POST.get('date'),
                                                email=request.POST.get(
                                                    'email'),
                                                modificationdate=datetime.now())
        messages.success(request, "Updated Successfully")
        context['pagetitle'] = "Department Details"
        return HttpResponseRedirect("/member/exammember")
    except Exception as details:
        print(details)
        messages.error(request, "Not Updated")
        elogger.error("Error while updating an department %s" % (details))
        return HttpResponseRedirect("/member/exammember")


@login_required
def DeleteISO(request, id):
    try:
        record = get_object_or_404(ISO, id=id)
        record.delete()
        messages.success(request, '%s iso deleted successfully' %
                         (record.name))
        ilogger.info('iso %s deleted by %s' %
                     (record.name, request.user))
        return HttpResponseRedirect("/member/iso")
    except Exception as details:
        elogger.error(
            'Trying to delete Deleteiso id: %s no such record' % (id))
        return HttpResponseRedirect("/member/iso")


@login_required
def EditISO(request, id):
    member = ISO.objects.get(id=id)
    initial_dict = {
        'departmentid': member.departmentid,
        'departmentcode': member.departmentcode,
        'organisationaname': member.organisationaname,
        'departmentname': member.departmentname,
        'nameiso': member.nameiso,
        'designation': member.designation,
        'phone_number': member.phone_number,
        'date': member.date,
        'email': member.email,
        'status': member.status
        
    }
    # add the dictionary during initialization

    form = ISOForm(request.POST or None, initial=initial_dict)
    context = {}
    context['form'] = form
    context['id'] = id
    context['pageicon'] = "fa-group"
    context['pagetitle'] = "ISO Details"
    return render(request, "login/editiso.html", context)


@login_required
def UpdateISO(request, id):
    context = {}
    dbdata = ISO.objects.get(id=id)
    try:
        ISO.objects.filter(id=id).update(departmentid=request.POST.get('departmentid'),
                                         departmentcode=request.POST.get(
            'departmentcode'),
            organisationaname=request.POST.get(
            'organisationaname'),
            departmentname=request.POST.get(
            'departmentname'),
            nameiso=request.POST.get('nameiso'),
            designation=request.POST.get(
            'designation'),
            phone_number=request.POST.get(
            'phone_number'),
            date=request.POST.get(
            'date'),
            email=request.POST.get(
            'email'),
            status=request.POST.get(
            'status'),
            modificationdate=datetime.now())
        messages.success(request, "Updated Successfully")
        context['pagetitle'] = "ISO Details"
        return HttpResponseRedirect("/member/iso")
    except Exception as details:
        print(details)
        messages.error(request, "Not Updated")
        elogger.error("Error while updating an exam member %s" % (details))
        return HttpResponseRedirect("/member/iso")


@login_required
def DeleteAsset(request, id):
    try:
        record = get_object_or_404(Asset, id=id)
        record.delete()
        messages.success(request, '%s Asset deleted successfully' %
                         (record.assetname))
        ilogger.info('member %s deleted by %s' %
                     (record.assetname, request.user))
        return HttpResponseRedirect("/member/asset")
    except Exception as details:
        elogger.error(
            'Trying to delete DeleteAsset id: %s no such record' % (id))
        return HttpResponseRedirect("/member/asset")


@login_required
def EditAsset(request, id):
    member = Asset.objects.get(id=id)
    initial_dict = {
        'organisationaname': member.organisationaname,
        'departmentname': member.departmentname,
        'url': member.url,
        'tnsdcip': member.tnsdcip,
        'nms': member.nms,
        'nmsdate': member.nmsdate,
        'assetusage': member.assetusage,
        'othersdetails': member.othersdetails,
        'departmentcontact': member.departmentcontact,
        'developer': member.developer,
        'siem': member.siem,
        'siemdate': member.siemdate

    }
    # add the dictionary during initialization

    form = AssetForm(request.POST or None, initial=initial_dict)
    context = {}
    context['form'] = form
    context['id'] = id
    context['pageicon'] = "fa-group"
    context['pagetitle'] = "Asset Details"
    return render(request, "login/editasset.html", context)


@login_required
def UpdateAsset(request, id):
    context = {}
    dbdata = Asset.objects.get(id=id)
    try:
        Asset.objects.filter(id=id).update(organisationaname=request.POST.get('organisationaname'),
                                           departmentname=request.POST.get(
            'departmentname'),
            url=request.POST.get(
            'url'),
            date=request.POST.get(
            'date'),
            tnsdcip=request.POST.get('tnsdcip'),
            nms=request.POST.get(
            'nms'),
            nmsdate=request.POST.get(
            'nmsdate'),
            assetusage=request.POST.get(
            'assetusage'),
            othersdetails=request.POST.get('othersdetails'),
            departmentcontact=request.POST.get(
            'departmentcontact'),
            developer=request.POST.get(
            'developer'),
            siem=request.POST.get(
            'siem'),
            siemdate=request.POST.get('siemdate'),
            modificationdate=datetime.now())
        messages.success(request, "Updated Successfully")
        context['pagetitle'] = "Asset Details"
        return HttpResponseRedirect("/member/asset")
    except Exception as details:
        print(details)
        messages.error(request, "Not Updated")
        elogger.error("Error while updating an exam member %s" % (details))
        return HttpResponseRedirect("/member/asset")


@login_required
def Deleteciso(request, id):
    try:
        record = get_object_or_404(CISO, id=id)
        record.delete()
        messages.success(request, '%s CISO deleted successfully' %
                         (record.nameciso))
        ilogger.info('member %s deleted by %s' %
                     (record.nameciso, request.user))
        return HttpResponseRedirect("/member/ciso")
    except Exception as details:
        elogger.error(
            'Trying to delete DeleteAsset id: %s no such record' % (id))
        return HttpResponseRedirect("/member/ciso")


@login_required
def Editciso(request, id):
    member = CISO.objects.get(id=id)
    initial_dict = {
        'departmentid': member.departmentid,
        'departmentcode': member.departmentcode,
        'organisationaname': member.organisationaname,
        'nameciso': member.nameciso,
        'designation': member.designation,
        'phone_number': member.phone_number,
        'date': member.date,
        'email': member.email,
        'status': member.status

    }
    # add the dictionary during initialization

    form = CISOForm(request.POST or None, initial=initial_dict)
    context = {}
    context['form'] = form
    context['id'] = id
    context['pageicon'] = "fa-group"
    context['pagetitle'] = "CISO Details"
    return render(request, "login/editciso.html", context)


@login_required
def Updateciso(request, id):
    context = {}
    dbdata = CISO.objects.get(id=id)
    try:
        CISO.objects.filter(id=id).update(departmentid=request.POST.get('departmentid'),
                                         departmentcode=request.POST.get(
            'departmentcode'),
            organisationaname=request.POST.get(
            'organisationaname'),
            nameciso=request.POST.get('nameciso'),
            designation=request.POST.get(
            'designation'),
            phone_number=request.POST.get(
            'phone_number'),
            date=request.POST.get(
            'date'),
            email=request.POST.get(
            'email'),
            status=request.POST.get(
            'status'),
            modificationdate=datetime.now())

        messages.success(request, "Updated Successfully")
        context['pagetitle'] = "CISO Details"
        return HttpResponseRedirect("/member/ciso")
    except Exception as details:
        print(details)
        messages.error(request, "Not Updated")
        elogger.error("Error while updating an CISO %s" % (details))
        return HttpResponseRedirect("/member/ciso")


@login_required
def DeleteVul(request, id):
    try:
        record = get_object_or_404(Vulnerability, id=id)
        record.delete()
        messages.success(request, '%s Vulnerability deleted successfully' %
                         (record.vulid))
        ilogger.info('member %s deleted by %s' %
                     (record.vulid, request.user))
        return HttpResponseRedirect("/member/vul")
    except Exception as details:
        elogger.error(
            'Trying to delete DeleteAsset id: %s no such record' % (id))
        return HttpResponseRedirect("/member/vul")


@login_required
def EditVul(request, id):
    member = Vulnerability.objects.get(id=id)
    initial_dict = {
        'departmentname': member.departmentname,
        'vulname': member.vulname,
        'vulid': member.vulid,
        'reportby': member.reportby,
        'reporteddate': member.reporteddate

    }
    # add the dictionary during initialization

    form = VulnerabilityForm(request.POST or None, initial=initial_dict)
    context = {}
    context['form'] = form
    context['id'] = id
    context['pageicon'] = "fa-group"
    context['pagetitle'] = "Vulnerability Details"
    return render(request, "login/editvul.html", context)


@login_required
def UpdateVul(request, id):
    context = {}
    try:
        dbdata = Vulnerability.objects.get(id=id)
        dbdata.id = None
        dbdata.active = False
        dbdata.save()
    
        Vulnerability.objects.filter(id=id).update(departmentname=request.POST.get('departmentname'),
                                          vulid=request.POST.get(
            'vulid'),
            vulname=request.POST.get(
            'vulname'),
            reportby=request.POST.get(
            'reportby'),
            reporteddate=request.POST.get(
            'reporteddate'),
            modificationdate=datetime.now(),
            modified_user = request.user)

        messages.success(request, "Updated Successfully")
        context['pagetitle'] = "Vulnerability Details"
        
        return HttpResponseRedirect("/member/vul")
    except Exception as details:
        print(details)
        messages.error(request, "Not Updated")
        elogger.error("Error while updating an Vulnerability %s" % (details))
        return HttpResponseRedirect("/member/vul")

@login_required
def DeleteVul1(request, id):
    try:
        record = get_object_or_404(Vulnerability, id=id)
        record.delete()
        messages.success(request, '%s Vulnerability deleted successfully' %
                         (record.vulid))
        ilogger.info('member %s deleted by %s' %
                     (record.vulid, request.user))
        return HttpResponseRedirect("/member/vul1")
    except Exception as details:
        elogger.error(
            'Trying to delete DeleteAsset id: %s no such record' % (id))
        return HttpResponseRedirect("/member/vul1")


@login_required
def EditVul1(request, id):
    member = Vulnerability.objects.get(id=id)
    initial_dict = {
        'departmentname': member.departmentname,
        'vulname': member.vulname,
        'vulid': member.vulid,
        'reportby': member.reportby,
        'reporteddate': member.reporteddate

    }
    # add the dictionary during initialization

    form = VulnerabilityForm(request.POST or None, initial=initial_dict)
    context = {}
    context['form'] = form
    context['id'] = id
    context['pageicon'] = "fa-group"
    context['pagetitle'] = "Vulnerability Details"
    return render(request, "login/editvul1.html", context)


@login_required
def UpdateVul1(request, id):
    context = {}
    try:
        dbdata = Vulnerability.objects.get(id=id)
        dbdata.id = None
        dbdata.active = False
        dbdata.save()
    
        Vulnerability.objects.filter(id=id).update(departmentname=request.POST.get('departmentname'),
                                          vulid=request.POST.get(
            'vulid'),
            vulname=request.POST.get(
            'vulname'),
            reportby=request.POST.get(
            'reportby'),
            reporteddate=request.POST.get(
            'reporteddate'),
            modificationdate=datetime.now(),
            modified_user = request.user)

        messages.success(request, "Updated Successfully")
        context['pagetitle'] = "Vulnerability Details"
        
        return HttpResponseRedirect("/member/vul1")
    except Exception as details:
        print(details)
        messages.error(request, "Not Updated")
        elogger.error("Error while updating an Vulnerability %s" % (details))
        return HttpResponseRedirect("/member/vul1")        
