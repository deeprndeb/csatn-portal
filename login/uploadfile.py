from django.http import HttpResponseRedirect
from django.views.generic import View
from django.core.files.storage import FileSystemStorage
from .models import ExamMember
import time
import xlrd
from django.contrib import messages
from django.conf import settings
from django.db import transaction
from django.core.exceptions import ValidationError
import re
import os, shutil
import py7zr
import subprocess


class UploadMemberXls(View):
    def post(self, request):
        if request.FILES['datafile']:
            myfile = request.FILES['datafile']
            fs = FileSystemStorage()

            # FileName with EPOC time format
            epoc = int(time.time())
            #tmpfileName = str(request.user)+"-"+str(epoc)+".csv"
            filename = fs.save(myfile.name, myfile)
            try:
                wb = xlrd.open_workbook(settings.MEDIA_ROOT+"/"+filename)
                sheet = wb.sheet_by_index(0)
                with transaction.atomic():
                    for row in range(1, (sheet.nrows)):
                        member = ExamMember()
                        member.departmentid = str(sheet.cell_value(row, 0))
                        if len(member.departmentid) == 0:
                            messages.error(request, "row:%s column:%s please enter correct member name %s" % (
                                row+1, 1+1, member.departmentid))
                            os.remove(os.path.join(
                                settings.MEDIA_ROOT, filename))
                            return HttpResponseRedirect('/exam/exammember')
                        member.departmentcode = str(sheet.cell_value(row, 1))
                        member.organisationaname = str(sheet.cell_value(row,2))
                        member.departmentname = str(sheet.cell_value(row, 3))
                        member.phone_number = str(sheet.cell_value(row, 4))
                        member.date = str(sheet.cell_value(row, 5))
                        member.email = str(sheet.cell_value(row, 6))
                        member.modificationdate = str(sheet.cell_value(row, 7))
                                             
                        member.save()
                    messages.info(request, "File Uploaded")
                    return HttpResponseRedirect('/exam/exammember')
            except Exception as details:
                print(details)
                messages.error(
                    request, "Exam Member Data be in-correct format or order")
                return HttpResponseRedirect('/exam/exammember')
        else:
            messages.error(request, "File Not Uploaded")
            return HttpResponseRedirect('/exam/exammember')

